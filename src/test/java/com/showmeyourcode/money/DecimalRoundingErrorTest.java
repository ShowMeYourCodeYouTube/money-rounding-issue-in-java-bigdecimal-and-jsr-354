package com.showmeyourcode.money;


import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

class DecimalRoundingErrorTest {

    @Test
    void shouldRunTheMainMethodWithoutErrors() {
        assertDoesNotThrow(() ->
                DecimalRoundingError.main(new String[]{})
        );
    }
}
